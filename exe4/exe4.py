#! /usr/bin/python3
from datetime import date

def halfLife(firstDate, secondDate):
    #put into date object
    firstDate = date.fromisoformat(firstDate)
    secondDate = date.fromisoformat(secondDate)

    #calculate the difference
    diff = firstDate - secondDate

    #add to the bigger date the difference
    if(diff.days < 0):
        return (secondDate + (-diff)).isoformat()
    return (firstDate + diff).isoformat()



print(halfLife("2000-05-01","1996-01-30"))


