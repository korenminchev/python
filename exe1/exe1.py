#! /usr/bin/python3
from collections import deque

def sortSmaller(a):
    #create an empty deque
    b = deque([])
    for num in a:
        #if number is negative, put it on the left
        if(num < 0):
            b.appendleft(num)
        #if positive put it on the right
        else:
            b.append(num)
    #return a list
    return list(b)

