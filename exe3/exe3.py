#! /usr/bin/python3

class defaultlist:
    def __init__(self, listArg, defaultValue):
        #make a copy of the list
        self._list = listArg[:]
        self._defaultValue = defaultValue


    def __getitem__(self, key):
        #if the index is out of boundary, return defaultValue
        if(key >= len(self._list)):
            return self._defaultValue
        else:
            return self._list[key]


    def __setitem__(self, key, value):
        #if the index is out of boundary extend the list with defaultValue
        if(key >= len(self._list)):
            self.extend([self._defaultValue]*(key - len(self._list) + 1))
        self._list[key] = value


    def extend(self, toExtend):
        self._list.extend(toExtend)


    def append(self, value):
        self._list.append(value)

    
    def insert(self, index, value):
        #if the index is out of boundary extend the list with defaultValue
        if(index >= len(self._list)):
            self.extend([self._defaultValue]*(index - len(self._list) + 1)
        self._list.insert(index, value)

    
    def remove(self, element):
        self._list.remove(element)

    
    def pop(self, index):
        return self._list.pop(index)


    def __str__(self):
        return self._list.__str__()

    
    def __repr__(self):
        return {"list":self._list, "defaultValue": self._defaultValue}
