#! /usr/bin/python3

def fibonacci(max_elements = float("inf"), max_value = float("inf")):
    #check if we got input
    if(max_elements == float("inf") and max_value == float("inf")):
        raise NotImplementedError

    #initial values
    value, nextValue = 0, 1
    count = 0

    #main loop
    while(value <= max_value and count < max_elements):
        yield value
        value, nextValue = nextValue, nextValue + value
        count += 1